import React, { useState } from 'react';

interface GitlabConfigurationPromptPageProps {
    onSubmit: (endpoint: string, accessToken: string) => void;
}

export function GitlabConfigurationPromptPage({
    onSubmit,
}: GitlabConfigurationPromptPageProps): JSX.Element {
    const [endpoint, setEndpoint] = useState('');
    const [accessToken, setAccessToken] = useState('');
    return (
        <div className="container">
            <form
                onSubmit={e => {
                    onSubmit(endpoint, accessToken);
                    e.preventDefault();
                }}
            >
                <div className="mb-3">
                    <label className="form-label" htmlFor="endpoint">
                        Endpoint
                    </label>
                    <input
                        className="form-control"
                        type="url"
                        placeholder="https://gitlab.com"
                        id="endpoint"
                        required={true}
                        value={endpoint}
                        onChange={e => {
                            setEndpoint(e.target.value);
                            e.preventDefault();
                        }}
                    />
                    <div className="form-text">
                        <a
                            href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html"
                            target="_blank"
                            rel="noreferrer"
                        >
                            More information about personal access tokens
                        </a>
                    </div>
                </div>

                <div className="mb-3">
                    <label htmlFor="access-token">Access Token:</label>
                    <input
                        className="form-control"
                        type="password"
                        id="access-token"
                        required={true}
                        onChange={e => {
                            setAccessToken(e.target.value);
                            e.preventDefault();
                        }}
                    />
                </div>

                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </form>
        </div>
    );
}
