import React from 'react';
import { gql, useQuery } from '@apollo/client';
import { partition, sortBy } from 'lodash';
import { useParams } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { MergeRequestDashboard } from '../components/MergeRequestDashboard';
import { MergeRequest } from '../types';

interface Nodes<T> {
    nodes: T[];
}

interface Project {
    archived: boolean;
    name: string;
}

interface GitlabMergeRequest {
    approved: boolean;
    createdAt: string;
    draft: boolean;
    id: string;
    title: string;
    webUrl: string;
    project: Project;
}

interface User {
    authoredMergeRequests: Nodes<GitlabMergeRequest>;
    id: string;
    name: string;
}

interface Group {
    groupMembers: Nodes<{ user: User }>;
}

interface MergeRequestData {
    group: Group | null;
}

const GET_MERGE_REQUEST_DATA = gql`
    query GetMergeRequestData($groupName: ID!) {
        group(fullPath: $groupName) {
            groupMembers {
                nodes {
                    user {
                        id
                        name
                        authoredMergeRequests(state: opened) {
                            nodes {
                                approved
                                createdAt
                                draft
                                id
                                title
                                webUrl
                                project {
                                    archived
                                    name
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

interface MergeRequestDashboardURLParams {
    groupName: string;
}

export function MergeRequestsForGroupPage(): JSX.Element {
    const { groupName } = useParams<MergeRequestDashboardURLParams>();

    const { loading, data, error } = useQuery<MergeRequestData>(GET_MERGE_REQUEST_DATA, {
        variables: { groupName },
    });
    if (loading) {
        return (
            <div>
                <p>Loading data...</p>
            </div>
        );
    }
    if (error) {
        return <div>{JSON.stringify(error)}</div>;
    }
    if (!data) {
        return (
            <div>
                <p>No data after loading!</p>
            </div>
        );
    }

    const { group } = data;

    if (!group) {
        return (
            <div className="container">
                <p>Group {groupName} does not exist.</p>
            </div>
        );
    }

    const mrs = sortBy(calculateMRs(group), 'createdAt');
    const [draftMrs, prodMrs] = partition(mrs, 'isDraft');

    return (
        <>
            <Helmet>
                <title>MR Dashboard: {groupName}</title>
            </Helmet>
            <MergeRequestDashboard
                draftMrs={draftMrs}
                prodMrs={prodMrs}
                title={`Active MRs for ${groupName}`}
            />
        </>
    );
}

function calculateMRs(group: Group): MergeRequest[] {
    return group.groupMembers.nodes
        .map(m => {
            const author = m.user.name;
            return m.user.authoredMergeRequests.nodes
                .filter(mr => !mr.project.archived)
                .map(mr => ({
                    author,
                    createdAt: mr.createdAt,
                    id: mr.id,
                    isApproved: mr.approved,
                    isDraft: mr.draft,
                    name: mr.title,
                    project: mr.project.name,
                    url: mr.webUrl,
                }));
        })
        .flat();
}
