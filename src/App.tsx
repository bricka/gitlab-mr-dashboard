import React, { useState } from 'react';

import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { isEmpty } from 'lodash';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { GitlabConfigurationPromptPage } from './pages/GitlabConfigurationPromptPage';
import { MergeRequestsForGroupPage } from './pages/MergeRequestsForGroupPage';

const GITLAB_ACCESS_TOKEN = 'GITLAB_ACCESS_TOKEN';
const GITLAB_ENDPOINT = 'GITLAB_ENDPOINT';

export function App(): JSX.Element {
    const [gitlabEndpoint, setGitlabEndpoint] = useState(localStorage.getItem(GITLAB_ENDPOINT));
    const [accessToken, setAccessToken] = useState(localStorage.getItem(GITLAB_ACCESS_TOKEN));

    if (isEmpty(gitlabEndpoint) || isEmpty(accessToken)) {
        return (
            <GitlabConfigurationPromptPage
                onSubmit={(endpoint, accessToken) => {
                    localStorage.setItem(GITLAB_ENDPOINT, endpoint);
                    localStorage.setItem(GITLAB_ACCESS_TOKEN, accessToken);

                    setGitlabEndpoint(endpoint);
                    setAccessToken(accessToken);
                }}
            />
        );
    }

    const client = new ApolloClient({
        uri: `${gitlabEndpoint}/api/graphql`,
        cache: new InMemoryCache(),
        headers: {
            authorization: `Bearer ${accessToken}`,
        },
    });

    return (
        <ApolloProvider client={client}>
            <BrowserRouter>
                <Switch>
                    <Route path="/groups/:groupName">
                        <MergeRequestsForGroupPage />
                    </Route>
                    <Route path="*">
                        <p>
                            No matching route. You probably want to go to{' '}
                            <code>/groups/GITLAB_GROUP_NAME</code>.
                        </p>
                    </Route>
                </Switch>
            </BrowserRouter>
        </ApolloProvider>
    );
}
