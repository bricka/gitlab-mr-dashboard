import React from 'react';
import { MergeRequest } from '../types';

import { MergeRequestTable } from './MergeRequestTable';

interface MergeRequestDashboardProps {
    draftMrs: MergeRequest[];
    prodMrs: MergeRequest[];
    title: string;
}

export function MergeRequestDashboard({
    draftMrs,
    prodMrs,
    title,
}: MergeRequestDashboardProps): JSX.Element {
    return (
        <div className="container">
            <h1 className="mt-3">{title}</h1>
            <MergeRequestTable mrs={prodMrs} />

            <h1>Draft MRs</h1>
            <MergeRequestTable mrs={draftMrs} />
        </div>
    );
}
