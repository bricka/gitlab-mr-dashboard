import React from 'react';
import { isEmpty } from 'lodash';
import { MergeRequest } from '../types';

interface MergeRequestTableProps {
    mrs: MergeRequest[];
}

const rtf = new Intl.RelativeTimeFormat('en');

const now = new Date();

function relativeTimeAgo(createdAt: string): string {
    const then = new Date(createdAt);
    const differenceInDays = Math.round((then.getTime() - now.getTime()) / 1000 / 60 / 60 / 24);
    return rtf.format(differenceInDays, 'day');
}

export function MergeRequestTable({ mrs }: MergeRequestTableProps): JSX.Element {
    if (isEmpty(mrs)) {
        return <div>No MRs to display</div>;
    }
    return (
        <div className="row">
            <div className="col">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Project</th>
                            <th />
                            <th>MR</th>
                            <th>Author</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        {mrs.map(m => (
                            <tr key={m.id}>
                                <td>{m.project}</td>
                                <td>
                                    {m.isApproved ? <span title="Already Approved">✅</span> : null}
                                </td>
                                <td>
                                    <a href={m.url} target="_blank" rel="noreferrer">
                                        {m.name}
                                    </a>
                                </td>
                                <td>{m.author}</td>
                                <td>{relativeTimeAgo(m.createdAt)}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
