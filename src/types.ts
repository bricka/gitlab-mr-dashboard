export interface MergeRequest {
    author: string;
    createdAt: string;
    id: string;
    isApproved: boolean;
    isDraft: boolean;
    name: string;
    project: string;
    url: string;
}
