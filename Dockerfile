FROM node:16-buster-slim AS builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
COPY tsconfig.json webpack.config.ts ./
COPY src src
COPY public public
RUN npm run build
RUN npm prune --production

FROM node:16-buster-slim

WORKDIR /app
COPY --from=builder /app/node_modules node_modules
COPY --from=builder /app/package.json .
COPY --from=builder /app/dist dist
CMD ["npm", "start"]
