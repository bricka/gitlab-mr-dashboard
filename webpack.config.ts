import * as path from 'path';
import * as webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import 'webpack-dev-server'; // We need the type definition to be imported

const config: webpack.Configuration = {
    entry: path.resolve(__dirname, 'src', 'index.tsx'),
    target: 'web',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[fullhash].js',
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
        modules: ['node_modules'],
        mainFields: ['main'],
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                loader: 'ts-loader',
            },
            {
                test: /\.(png|svg|eot|woff|woff2|ttf|pdf)$/,
                loader: 'file-loader',
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public', 'index.html'),
        }),
    ],
    devtool: 'cheap-module-source-map',
};

(config as any).devServer = {
    contentBase: path.resolve(__dirname, 'dist'),
    host: '0.0.0.0',
    port: 8080,
    disableHostCheck: true,
    headers: {
        'Access-Control-Allow-Origin': 'http://0.0.0.0:8080',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers':
            'Content-Type, Authorization, x-id, Content-Length, X-Requested-With',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
    },
    watchOptions: {
        ignored: ['./src/**/*test.tsx', './src/**/*test.ts', 'node_modules/**'],
    },
    historyApiFallback: true,
};

export default config;
