Welcome to the GitLab MR Dashboard. This tool helps you find open MRs for your groups, so you can see what's in need of review.

To use it, you probably want to go to the route `/groups/GITLAB_GROUP_NAME`. This will search for MRs opened by members of your group.

# Authentication

To use this dashboard, you need to provide a Gitlab access token that has the `read_api` permission. You can create it by following the instructions here: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html .

Your access token will be stored in the local storage of your browser.

# How It Works

Behind the scenes, the dashboard uses the [Gitlab GraphQL API](https://docs.gitlab.com/ee/api/graphql/). It starts at the given group, and then traverses the graph to get information about members of the group, their open MRs, and the projects those MRs are against.
